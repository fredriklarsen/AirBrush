# AirBrush changelog

## Version 1.4.1
- Remove library code obfuscation. Consumer can obfuscate when needed.
- Disable Jetifier (´android.enableJetifier=false´)
- Upgrade androidx annotations to 1.6.0

## Version 1.4.0
- Upgrade target and compile SDK to 33
- Upgrade to AGP 7.4
- Upgrade to Kotlin 1.8.10
- Upgrade to Coroutines 1.6.4
- Upgrade to AppCompat 1.6.0
- Upgrade to RecyclerView 1.2.1
- Upgrade to OkHttp 4.9.2
- Removed custom ProGuard rules to facilitate code obfuscation

## Version 1.3.0

- Changed groupId from `com.subgarden.android` to `com.subgarden.airbrush`
- Migrate to AGP 7
- Now targeting API 30
- Upgrade to Kotlin 1.5.21
- Upgrade to Coroutines 1.5.1
- Upgrade to Glide 4.12.0
- Upgrade sample app dependencies to latest stable version

*Since Bintray/JCenter is retired, artifacts are now published to Maven Central via Sonatype.* 

## Version 1.2.0

- Upgrade to kotlin 1.3.71
- Upgrade to Glide 4.11.0
- Fixes an issue with Closable resources not being closed properly (Thanks Alfredo Perez @alfredo_zedge)
- Improved cleanup of RenderScript allocations for both TinyThumb and GradientPalette.
- Explicitly release intermediary cropped bitmap for TinyThumb

Changes in the sample app:

- Added StrictMode to detect potential issues
- Now using kotlin coroutines to load the sample data source async
- Refactored to use ListAdapter (RecyclerView) and a general code cleanup

## Version 1.1.0

- Removed library AndroidManifest which included conflicting `app_name` resource value
- Removed decode measurement which theoretically degrades performance
- Bumped min version to follow semver (https://semver.org/) due to the custom blur API change in 1.0.1
- Upgraded to latest android gradle plugin version
- Upgraded to latest kotlin version
 
## Version 1.0.1

- Added support for custom blur options
- Increased compileSdk and targetSdk to 29
- Updated to latest dependencies
- Restructured and cleaned up to facilitate for some long overdue tests
- Migrated project to GitLab 

## Version 1.0.0

- The library is now considered stable 🎉
- Bumped minSdk and renderscriptTargetApi to 21 in order to support x86_64 ABI.
- Removed RenderScript support library (not required)
- Cleaned up dependency versions
- Migrated to AndroidX 


## Version 0.6.1

A few enhancements: 
- TinyThumbDecoder now supports a different default base64 decode flag.
- TinyThumb can now override the default base64 decode flag.
- Removed bitmapProvider from TinyThumbDecoder. A custom decoder class should be used instead.
- Added @JvmOverload to better support default arguments from Java
- Added more documentation
- Upgraded to latest kotlin and support library versions

## Version 0.6.0
Big rewrite for optimization.
- Using Glide 4 and custom loaders to for thumbnails.
- Using RenderScript to generate gradients.
- Renamed Palette to GradientPalette.
- Introducing TinyThumb which takes a base64 encoded JPEG. It's decoded and blurred at runtime.
- The library GlideModule handles registration of decoders/loaders, but thus can be customized in the App's GlideModule. See sample app for details.
- Updated sample app to reflect changes.

To generate a Bitmap from a GradientPalette:

```
AirBrush(context).getGradient(gradientPalette, width, height)
```

Note that it must be done on a worker thread.

### Image Loading

For loading TinyThumb or GradientPalette with Glide, load it directly.
For TinyThumb:

```
    val tinyThumb = getTinyThumb()
    requestManager
                .load(imageUri)
                .thumbnail(requestManager.load(tinyThumb))
                .into(imageView)
```

For GradientPalette:
```
    val gradientPalette = getGradientPalette()
    requestManager
                .load(imageUri)
                .thumbnail(requestManager.load(gradientPalette))
                .into(imageView)
```

### Utility methods

```
AirBrush.blur(context, bitmap, scale, radius)

AirBrush.getPalette(bitmap);
```


## Version 0.5.1 

First release available through maven.

To get a gradient drawable use 
```
Airbrush.getGradient(imageView, palette);
```

There is also a utility method for creating a Palette based on a bitmap. This can be very handy while developing. 

```
AirBrush.getPalette(bitmap);
```

_Note:_ Since AirBrush is using a ComposeShader inside a ComposeShader, the view layer has to be rendered in software. 
AirBrush does this for you by calling `ViewCompat.setLayerType(view, View.LAYER_TYPE_SOFTWARE, null);`



