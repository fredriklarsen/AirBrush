package com.subgarden.airbrush.sample

import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.subgarden.airbrush.AirBrush
import com.subgarden.airbrush.loaders.GradientPalette
import com.subgarden.airbrush.loaders.TinyThumb
import com.subgarden.airbrush.sample.databinding.ActivitySimpleBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SimpleActivity : AppCompatActivity() {

    companion object {
        const val SPAN_COUNT = 3
    }

    private lateinit var adapter: GridAdapter
    private var dataSource: StaticDataSource? = null

    private lateinit var binding: ActivitySimpleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimpleBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        // Detect any potential issues early.
        // If you see a StrictMode violation, please make an issue in GitLab.
        StrictMode.setThreadPolicy(
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build()
        )
        StrictMode.setVmPolicy(
            VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build()
        )

        binding.gradientPaletteRadioButton.text = "GradientPalette"
        binding.gradientPaletteRadioButton.isEnabled = false

        binding.tinyThumbRadioButton.text = "TinyThumb"
        binding.tinyThumbRadioButton.isEnabled = false

        adapter = GridAdapter(GlideApp.with(this))
        binding.recyclerView.adapter = adapter

        // The data source takes time to initialise. Do it in the background to satisfy StrictMode.
        GlobalScope.launch {
            dataSource = StaticDataSource(this@SimpleActivity).also {
                withContext(Dispatchers.Main) {
                    adapter.submitList(it.gradientPaletteData)

                    binding.gradientPaletteRadioButton.isEnabled = true
                    binding.tinyThumbRadioButton.isEnabled = true

                    binding.radioGroup.check(binding.gradientPaletteRadioButton.id)
                }
            }
        }

        binding.gradientPaletteRadioButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                dataSource?.let { adapter.submitList(it.gradientPaletteData) }
            }
        }

        binding.tinyThumbRadioButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                dataSource?.let { adapter.submitList(it.tinyThumbData) }
            }
        }

        val layoutManager = GridLayoutManager(this, SPAN_COUNT)
        binding.recyclerView.layoutManager = layoutManager

        binding.reloadButton.setOnClickListener {
            // Invalidate all items.
            // Normally notifyDataSetChanged would be used, but Glide's crossfade sometimes continues
            // after onBind is called again, causing a glitch.
            // This is normally not a problem, but for demo purposes it looks less than ideal.
            binding.recyclerView.adapter = adapter
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // After spraying paint all over the place it's time to clean up.
        // Luckily this is quick and easy. Normally you'd call cleanup() when you're down using AirBrush
        // for a good while. Note that it's safe to use AirBrush again, even after cleanup() has been called.
        AirBrush.cleanup()
    }
}
