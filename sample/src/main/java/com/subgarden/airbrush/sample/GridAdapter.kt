package com.subgarden.airbrush.sample

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.subgarden.airbrush.sample.databinding.GridLayoutItemBinding

/**
 * @author Fredrik Larsen (fredrik@subgarden.com)
 */
open class GridAdapter(
    private val requestManager: RequestManager,
) : ListAdapter<Item, GridAdapter.ViewHolder>(DiffCallback()) {

    class ViewHolder(
        view: View,
    ) : RecyclerView.ViewHolder(view) {
        val binding = GridLayoutItemBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.grid_layout_item, parent, false)
        val width = parent.resources.displayMetrics.widthPixels / SimpleActivity.SPAN_COUNT
        val height = parent.resources.displayMetrics.heightPixels / SimpleActivity.SPAN_COUNT

        return ViewHolder(view).apply {
            binding.image.layoutParams.width = width
            binding.image.layoutParams.height = height
        }
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = getItem(position)

        // Crossfade between the thumbnail and the final image
        val crossFade = DrawableTransitionOptions().crossFade(750)

        // For demo purposes we skip memory/disk cache
        val options = RequestOptions().apply {
            skipMemoryCache(true)
            diskCacheStrategy(DiskCacheStrategy.NONE)
        }

        val thumbnail: Any = when (item) {
            is GradientPaletteItem -> item.palette
            is TinyThumbItem -> item.thumb
        }

        // Load the image using the URI.
        // The delayed interceptor will return the drawable-nodpi resource id as a jpeg
        requestManager.load("http://airbrush/${item.resourceId}")
            // Notice how we load the types directly as a thumbnail in Glide
            .thumbnail(requestManager.load(thumbnail))
            .transition(crossFade)
            .apply(options)
            .into(holder.binding.image)
    }

}
